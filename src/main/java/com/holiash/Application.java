package com.holiash;

import com.holiash.binarytree.BinaryTreeMap;
import com.holiash.dequeue.ArrayDeque;

public class Application {

  public static void main(String[] args) {
    System.out.println("testing deque: ");
    testDeque();
    System.out.println("testing tree: ");
    testBinaryTree();
  }

  static void testDeque() {
    ArrayDeque<Integer> deque = new ArrayDeque<>();
    deque.addFirst(5);
    deque.addLast(8);
    deque.addFirst(7);
    System.out.println(deque.getFirst());
    System.out.println(deque.getLast());
    deque.addLast(15);
    deque.removeFirst();
    deque.removeLast();
    System.out.println(deque);
  }

  static void testBinaryTree() {
    BinaryTreeMap<Integer, String> map = new BinaryTreeMap<>();
    map.put(5, "five");
    map.put(2, "two");
    map.put(7, "seven");
    map.put(4, "four");
    map.put(6, "six");
    map.put(1, "one");
    map.draw();
    map.remove(5);
    map.draw();
    map.remove(6);
    map.draw();
    map.remove(1);
    map.draw();
    map.remove(4);
    map.draw();
    map.remove(2);
    map.draw();
    map.remove(7);
    map.draw();
  }
}
