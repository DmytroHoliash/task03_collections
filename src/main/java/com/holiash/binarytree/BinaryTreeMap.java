package com.holiash.binarytree;

import java.util.Comparator;
import java.util.Objects;

public class BinaryTreeMap<K, V> implements Map<K, V> {

  private final Comparator<? super K> comparator;
  private Pair<K, V> root;
  private int size = 0;

  public BinaryTreeMap() {
    this(null);
  }

  public BinaryTreeMap(Comparator<? super K> comparator) {
    this.comparator = comparator;
  }

  static final class Pair<K, V> {

    K key;
    V value;
    Pair<K, V> left;
    Pair<K, V> right;
    Pair<K, V> parent;

    Pair(K key, V value, Pair<K, V> parent) {
      this.key = key;
      this.value = value;
      this.parent = parent;
    }

    public K getKey() {
      return key;
    }

    public V getValue() {
      return value;
    }

    public void setValue(V value) {
      this.value = value;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (!(o instanceof Pair)) {
        return false;
      }
      Pair<?, ?> pair = (Pair<?, ?>) o;
      return getKey().equals(pair.getKey()) &&
          getValue().equals(pair.getValue());
    }

    @Override
    public int hashCode() {
      return Objects.hash(getKey(), getValue());
    }

    @Override
    public String toString() {
      return "(" + key + " = " + value + ")";
    }
  }

  @Override
  public int size() {
    return this.size;
  }

  @Override
  public boolean isEmpty() {
    return (size == 0);
  }

  @Override
  public boolean containsKey(K key) {
    return (get(key) != null);
  }

  @Override
  public Pair<K, V> get(K key) {
    if (key == null) {
      throw new NullPointerException();
    }
    if (comparator != null) {
      return getUsingComparator(key);
    }
    Comparable<? super K> k = (Comparable<? super K>) key;
    Pair<K, V> p = root;
    while (p != null) {
      int compare = k.compareTo(p.key);
      if (compare > 0) {
        p = p.right;
      } else if (compare < 0) {
        p = p.left;
      } else {
        return p;
      }
    }
    return null;
  }

  private Pair<K, V> getUsingComparator(K key) {
    Pair<K, V> p = root;
    while (p != null) {
      int compare = comparator.compare(key, p.key);
      if (compare > 0) {
        p = p.right;
      } else if (compare < 0) {
        p = p.left;
      } else {
        return p;
      }
    }
    return null;
  }

  @Override
  public boolean put(K key, V value) {
    if (root == null) {
      root = new Pair<>(key, value, null);
      size++;
      return true;
    }
    Pair<K, V> current = root;
    Pair<K, V> parent;
    int compare;
    if (comparator != null) {
      do {
        parent = current;
        compare = comparator.compare(key, current.key);
        if (compare > 0) {
          current = current.right;
        } else if (compare < 0) {
          current = current.left;
        } else {
          current.setValue(value);
          return true;
        }
      } while (current != null);
    } else {
      Comparable<? super K> k = (Comparable<? super K>) key;
      do {
        parent = current;
        compare = k.compareTo(current.key);
        if (compare > 0) {
          current = current.right;
        } else if (compare < 0) {
          current = current.left;
        } else {
          current.setValue(value);
          return true;
        }
      } while (current != null);
    }
    Pair<K, V> toInsert = new Pair<>(key, value, parent);
    if (compare > 0) {
      parent.right = toInsert;
    } else {
      parent.left = toInsert;
    }
    size++;
    return true;
  }

  @Override
  public V remove(K key) {
    if (key == null) {
      throw new NullPointerException();
    }
    Pair<K, V> pairToRemove = get(key);
    if (pairToRemove == null) {
      return null;
    }
    V result = pairToRemove.getValue();
    if (pairToRemove.equals(root)) {
      removeRoot(pairToRemove);
    } else {
      boolean isRightChild =
          (pairToRemove.parent.right != null) && pairToRemove.parent.right.equals(pairToRemove);
      if (pairToRemove.left == null && pairToRemove.right == null) {
        removeLeaf(pairToRemove, isRightChild);
      } else if (pairToRemove.right == null) {
        Pair<K, V> newChild = pairToRemove.left;
        removeWithOneChild(pairToRemove, isRightChild, newChild);
      } else if (pairToRemove.left == null) {
        Pair<K, V> newChild = pairToRemove.right;
        removeWithOneChild(pairToRemove, isRightChild, newChild);
      } else {
        removeWithBothChildren(pairToRemove, isRightChild);
      }
    }
    size--;
    return result;
  }

  private void removeWithBothChildren(Pair<K, V> pairToRemove, boolean isRightChild) {
    Pair<K, V> successor = getSuccessor(pairToRemove);
    if (isRightChild) {
      pairToRemove.parent.right = successor;
    } else {
      pairToRemove.parent.left = successor;
    }
    successor.parent = pairToRemove.parent;
  }

  private Pair<K, V> getSuccessor(Pair<K, V> pairToRemove) {
    Pair<K, V> successor = pairToRemove;
    Pair<K, V> current = pairToRemove.right;
    while (current != null) {
      successor = current;
      current = current.left;
    }
    if (successor != pairToRemove.right) {
      successor.parent.left = successor.right;
      successor.right = pairToRemove.right;
    }
    return successor;
  }

  private void removeWithOneChild(Pair<K, V> pairToRemove, boolean isRightChild,
      Pair<K, V> newChild) {
    if (isRightChild) {
      pairToRemove.parent.right = newChild;
    } else {
      pairToRemove.parent.left = newChild;
    }
    newChild.parent = pairToRemove.parent;
  }

  private void removeLeaf(Pair<K, V> pairToRemove, boolean isRightChild) {
    if (isRightChild) {
      pairToRemove.parent.right = null;
    } else {
      pairToRemove.parent.left = null;
    }
  }

  private void removeRoot(Pair<K, V> pairToRemove) {
    if (size == 1) {
      root = null;
    } else if ((root.left != null) && (root.right != null)) {
      Pair<K, V> successor = getSuccessor(pairToRemove);
      successor.left = root.left;
      root.left.parent = successor;
      successor.parent = null;
      root = successor;
    } else if (root.left != null) {
      root = root.left;
    } else if (root.right != null) {
      root = root.right;
    }
  }

  @Override
  public void clear() {
    this.root = null;
    size = 0;
  }

  private void inOrder(Pair<K, V> localRoot) {
    if (localRoot != null) {
      inOrder(localRoot.left);
      System.out.printf("%s ", localRoot);
      inOrder(localRoot.right);
    }
  }

  public void draw() {
    inOrder(root);
    System.out.println();
  }
}
