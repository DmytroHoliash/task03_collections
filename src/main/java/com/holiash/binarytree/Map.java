package com.holiash.binarytree;

import com.holiash.binarytree.BinaryTreeMap.Pair;

public interface Map<K, V> {
  int size();
  boolean isEmpty();
  boolean containsKey(K key);
  Pair<K, V> get(K key);
  boolean put(K key, V val);
  V remove(K key);
  void clear();
}
