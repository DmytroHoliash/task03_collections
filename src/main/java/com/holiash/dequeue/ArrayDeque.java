package com.holiash.dequeue;

import java.util.Arrays;

public class ArrayDeque<E> implements Deque<E> {

  private static final int DEFAULT_INITIAL_CAPACITY = 16;
  private Object[] deque;
  private int size;
  private final int head;
  private int tail;

  public ArrayDeque() {
    this(DEFAULT_INITIAL_CAPACITY);
  }

  public ArrayDeque(int initialCapacity) {
    if (initialCapacity < 1) {
      throw new IllegalArgumentException();
    }
    this.deque = new Object[initialCapacity];
    this.size = 0;
    this.head = 0;
    this.tail = -1;
  }


  @Override
  public void addFirst(E e) {
    if (e == null) {
      throw new NullPointerException();
    }
    if (size >= deque.length) {
      sizeUp();
    }
    if (size == 0) {
      deque[0] = e;
    } else {
      Object prev = deque[0];
      Object tmp;
      deque[0] = e;
      for (int i = 1; i < size; i++) {
        tmp = deque[i];
        deque[i] = prev;
        prev = tmp;
      }
      deque[size] = prev;
    }
    tail++;
    size++;
  }

  private void sizeUp() {
    deque = Arrays.copyOf(deque, deque.length * 2);
  }

  @Override
  public void addLast(E e) {
    if (e == null) {
      throw new IllegalArgumentException();
    }
    deque[size] = e;
    size++;
    tail++;
  }

  @Override
  public E removeFirst() {
    if (size == 0) {
      return null;
    }
    E result = (E) deque[head];
    int length = deque.length;
    deque = Arrays.copyOf(Arrays.copyOfRange(deque, head + 1, tail + 1), length);
    size--;
    tail--;
    return result;
  }

  @Override
  public E removeLast() {
    if (size == 0) {
      return null;
    }
    E result = (E) deque[tail];
    deque[tail] = null;
    tail--;
    size--;
    return result;
  }

  @Override
  public E getFirst() {
    if (size == 0) {
      return null;
    }
    return (E) deque[head];
  }

  @Override
  public E getLast() {
    if (size == 0) {
      return null;
    }
    return (E) deque[tail];
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("[");
    for (int i = 0; i < size; i++) {
      sb.append((E) deque[i]);
      if (i < size - 1) {
        sb.append(", ");
      }
    }
    sb.append("]");
    return sb.toString();
  }
}
