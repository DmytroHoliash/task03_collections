package com.holiash.priorityqueue;

import java.util.Arrays;
import java.util.Comparator;

public class PriorityQueue<E> implements Queue<E> {

  private static final int DEFAULT_INITIAL_CAPACITY = 11;

  private Object[] queue;
  private int size;
  private Comparator<? super E> comparator;

  public PriorityQueue() {
    this(DEFAULT_INITIAL_CAPACITY, null);
  }

  public PriorityQueue(int initialCapacity) {
    this(initialCapacity, null);
  }

  public PriorityQueue(Comparator<? super E> comparator) {
    this(DEFAULT_INITIAL_CAPACITY, comparator);
  }

  public PriorityQueue(int initialCapacity, Comparator<? super E> comparator) {
    if (initialCapacity < 1) {
      throw new IllegalArgumentException();
    }
    this.queue = new Object[initialCapacity];
    this.size = 0;
    this.comparator = null;
  }

  @Override
  public boolean offer(E e) {
    if (e == null) {
      return false;
    }
    if (size >= queue.length) {
      this.sizeUp();
    }
    if (comparator != null) {
      insertUsingComparator(e);
    } else {
      insertComparable(e);
    }
    size++;
    return true;
  }

  @Override
  public E poll() {
    if (size == 0) {
      return null;
    }
    E result = (E) queue[0];
    size--;
    if ((size < queue.length / 2) && (size > DEFAULT_INITIAL_CAPACITY)) {
      sizeDown();
    }
    refill();
    return result;
  }

  private void sizeDown() {
    queue = Arrays.copyOf(queue, queue.length / 2);
  }

  private void refill() {
    for (int i = 0; i < size; i++) {
      queue[i] = queue[i + 1];
    }
    queue[size] = null;
  }

  @Override
  public E peek() {
    if (size == 0) {
      return null;
    }
    return (E) queue[0];
  }

  private void insertComparable(E x) {
    Comparable<? super E> key = (Comparable<? super E>) x;
    int i = size;
    while (i > 0) {
      int parent = i - 1;
      Object e = queue[parent];
      if (key.compareTo((E) e) >= 0) {
        break;
      }
      queue[i] = e;
      i = parent;
    }
    queue[i] = key;
  }

  private void insertUsingComparator(E x) {
    int i = size;
    while (i > 0) {
      int parent = i - 1;
      Object e = queue[parent];
      if (comparator.compare(x, (E) e) >= 0) {
        break;
      }
      queue[i] = e;
      i = parent;
    }
    queue[i] = x;
  }

  private void sizeUp() {
    int newCapacity = (int) (queue.length * 1.5 + 1);
    queue = Arrays.copyOf(queue, newCapacity);
  }

  public int size() {
    return this.size;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("[");
    for (int i = 0; i < size; i++) {
      sb.append((E) queue[i]);
      if (i < size - 1) {
        sb.append(", ");
      }
    }
    sb.append("]");
    return sb.toString();
  }
}
